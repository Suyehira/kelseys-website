<?php
require_once "../dynamic/dao.php";

class User {

	private $id;			//int
	private $name;			//string
	private $email;			//string
	private $password;		//int
	private $about;			//string
	private $picture;		//string
	private $tasks = array();	//array of the user's tasks
	private $sched = array();	//array of the user's schedules

	public function __construct($email,$password){
		$this->email = $email;
		$this->password = $password;
		$dao = new Dao();
		$idArray = $dao->getUserId($email);
		$this->id = $idArray["id"];
		$this->tasks = $dao->getTasks($this->id);
		$this->sched = $dao->getSched($this->id);
	}

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getEmail() {
		return $this->email;
	}

	public function getPassword(){
		return $this->password;
	}

	public function getAbout() {
		return $this->about;
	}

	public function getPicture() {
		return $this->picture;
	}

	public function getTasks(){
		return $this->tasks;
	}


	public function getTask($taskId){
		foreach ($this->tasks as $task) {
			if ($task->getId() === $taskId) {
				return $task;
			}				
		}
		return null;
	}


	public function getSched(){
		return $this->sched;
	}


	public function setName($name) {
		$this->name = $name;
	}

	public function setEmail($email) {
		$this->email = $email;
	}

	public function setAbout($about) {
		$this->about = $about;
	}

	public function setPicture($picture) {
		$this->picture = $picture;
	}

	public function addTask($task) {
		array_push($this->tasks,$task);
	}

	public function addSched($sched) {
		array_push($this->sched, $sched);
	}

	public function deleteSched($sched) {
		foreach ($this->sched as $arrayKey => $arraySched) {
			if ($arraySched->getDay() == $sched->getDay() and
				$arraySched->getTaskId() == $sched->getTaskId()) {
					unset($this->sched[$arrayKey]);
			}
		}
	}
	public function schedArray() {
		$schedArr = array();
		foreach($this->sched as $array) {
			array_push($schedArr, $array->toArray());
		}
		return $schedArr;
	}
}

?>
