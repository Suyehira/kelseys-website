

<?php


class Schedule {

	private $day;
	private $label;
	private $taskId;
	private $amount;

	public function __construct($day, $label, $taskId, $amount=0) {
		$this->day = $day;
		$this->label = $label;
		$this->taskId = $taskId;
		$this->amount = $amount;
	}

	public function getDay(){
		return $this->day;
	}

	public function getTaskName(){
		return $this->label;
	}

	public function getTaskId(){
		return $this->taskId;
	}

	public function getAmount(){
		return $this->amount;
	}

	public function setDay($day){
		$this->day = $day;
	}

	public function setTaskId($taskId){
		$this->taskId = $taskId;
	}

	public function setAmount($amount){
		$this->amount = $amount;
	}

	public function toArray(){
		$arr = array (
			"day" => $this->day,
			"label" => $this->label,
			"taskId" => $this->taskId,
			"amount" => $this->amount,
		);
		return $arr;
	}

}






?>
