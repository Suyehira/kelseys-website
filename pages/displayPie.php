<?php
	require_once "../dynamic/SessionHelper.php";
	require_once "../classes/User.php";
	ensure_logged_in();
	$user = unserialize($_SESSION["user"]);
	$thisPage = "display";
	$display = "pie";
	include 'header.php';
?>
<script type="text/javascript">
	var jsonS = <?php echo json_encode($user->schedArray()); ?>;
</script>

		<div class="content">

			<div id="submenu">
				<ul>
					<li <?php if ($display=="pie") echo " id=\"currentDisplay\""; ?>>
						<a href="displayPie.php"> Pie Chart</a>
					</li>
					<li <?php if ($display=="bar") echo " id=\"currentDisplay\""; ?>>
						<a href="displayBar.php"> Bar Graph</a>
					</li>
				</ul>
			</div>
			<div class="select">
				<ul>
			<!--		<li>
						<select id="task">  <?php
					//	$tasks = $user->getTasks();
					//	foreach ($tasks as $task) {
					//		echo "<option value=\"" . $task->getId() . "\">" . $task->getName() . "</option>";
					//	} ?>
					</li> -->
					<li>
						Start Date:<input type="text" class="datepicker" name="startDateP"> 
					</li>
					<li>
						End Date:<input type="text" class="datepicker" name="endDateP">
					</li>
				</ul>
				<button id="pie">Refresh</button>
			</div>

			<div id="canvas_container_pie">
			</div>

		</div>

<?php include 'footer.php'; ?>
