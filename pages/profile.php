
<?php
	require_once "../dynamic/SessionHelper.php";
	require_once "../classes/User.php";
	ensure_logged_in();
	$user = unserialize($_SESSION["user"]);
	$thisPage = "profile";
	include 'header.php';
?>
	<body>

		<div class="content">

		<!--	<div class="profile">
				Welcome!
				<img src=""/>
				name <br/>
				email <br/>
				write someting about yourself... <br/>
				<button type="button">Edit</button>
			</div> -->

			<div class="top">
				Manage your labels...
			</div>

			<div class="labels">
			<table class="center">
				<tr><th>Name</th> <th>Category</th></tr>

			 <?php

			$tasks = $user->getTasks();
			foreach ($tasks as $task) {
				echo "<tr>";
				//NEED TO ESCAPE THESE
				echo "<td>" . htmlspecialchars($task->getName()) . "</td>";
				echo "<td>" . htmlspecialchars($task->getCategory()) . "</td>";
				echo "</tr>";
			  }

			?>

				<form action="../dynamic/profileHandler.php" method="POST">
					<tr><td><input type="text" placeholder="New Name" name="name"></td>
					<td><input type="text" placeholder="New Category" name="category"></td>
					<td><input type="submit" name="save" value="Save"></td>
				</form>
			</table>
			</div>
			
		</div>

	<?php include 'footer.php'; ?>

