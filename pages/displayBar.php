<?php
	require_once "../dynamic/SessionHelper.php";
	require_once "../classes/User.php";
	ensure_logged_in();
	$user = unserialize($_SESSION["user"]);
	$thisPage = "display";
	$display = "bar";
	$jsonSchedInfo = json_encode($user->schedArray());
	include 'header.php';
?>

<script type="text/javascript">
	var jsonS = <?php echo json_encode($user->schedArray()); ?>;
</script>
		<div class="content">

			<div id="submenu">
				<ul>
					<li <?php if ($display=="pie") echo " id=\"currentDisplay\""; ?>>
						<a href="displayPie.php"> Pie Chart</a>
					</li>
					<li <?php if ($display=="bar") echo " id=\"currentDisplay\""; ?>>
						<a href="displayBar.php"> Bar Graph</a>
					</li>
				</ul>
			</div>
			<div class="select">
				<ul>
					<li>
						Start Date:<input type="text" class="datepicker" name="startDateB">
					</li>
					<li>
						End Date:<input type="text" class="datepicker" name="endDateB">
					</li>
				</ul>
				<select id="task"> <?php
					$tasks = $user->getTasks();
					foreach ($tasks as $task) {
					echo "<option value=\"".htmlspecialchars($task->getId())."\">".htmlspecialchars($task->getName())."</option>";
				} ?> </select>

				<button id="bar">Refresh</button>
			</div>

			<div id="canvas_container_bar"></div>		

		</div>

<?php include 'footer.php'; ?>

