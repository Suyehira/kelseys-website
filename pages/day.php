<?php
	require_once "../dynamic/SessionHelper.php";
	require_once "../classes/User.php";
	require_once "../classes/Task.php";
	ensure_logged_in();
	$user = unserialize($_SESSION["user"]);
	$thisPage = "record";
	include 'header.php';
	if (isset($_GET["day"])) {
		$day = $_GET["day"];
		date_default_timezone_set("America/Boise");
		$day = date_create($day);
		$dayLabel = date_format($day, "l\, F jS");
	} else {
		date_default_timezone_set("America/Boise");
		$day = date("Y-m-d");
		$dayLabel = date("l\, F jS");
	}
	$_SESSION["currDay"] = $day->format('Y-m-d');
?>
<script type="text/javascript">
	var day = <?php echo json_encode($day); ?>;
</script>

	<div class="content">

		<div id="submenu">
			<ul>
				<li>
					<a id="previous" href="" >Previous</a>
				</li>
				<li>
					<a href="record.php"> Calendar </a>
				</li>
				<li>
					<a  id="next" href="">Next</a>
				</li>
			</ul>
		</div>

		<div class="input">
		<table class="center">
			<caption> <?php echo $dayLabel ?> </caption>
			<tr><th> Activity </th> <th> Time </th> </tr>
		
			 <?php
			$scheds = $user->getSched();
			foreach ($scheds as $sched) {
				if ($sched->getDay() == $day->format('Y-m-d')) {
					$tempTask = $user->getTask($sched->getTaskId()); 
					echo "<tr>";
					echo "<td>" . htmlspecialchars($tempTask->getName()) . "</td>";
					echo "<td>" . htmlspecialchars(((int) ($sched->getAmount()/60))) . " Hr </td>";
					echo "<td>" . htmlspecialchars($sched->getAmount()%60) . " Min </td>";
					echo "<form action=\"../dynamic/ScheduleHandler.php\" method=\"post\">";
					echo "<td><input type=\"submit\" name=\"delete\" value=\"Delete\"></td>";
					echo "<td><input type=\"hidden\" name=\"taskId\" value=\"" . htmlspecialchars($sched->getTaskId()) . "\"></td>";
					echo "<td><input type=\"hidden\" name=\"".htmlspecialchars($sched->getTaskId())."\" value=\"".htmlspecialchars($sched->getTaskName())."\">";
					echo "</form></tr>";
				}
			  }

			?>

			<form action="../dynamic/ScheduleHandler.php" method="post"> 
				<td> <select name="task">  <?php
					$tasks = $user->getTasks();
					foreach ($tasks as $task) {
					echo "<option value=\"".htmlspecialchars($task->getId())."|".htmlspecialchars($task->getName())."\">" . htmlspecialchars($task->getName()) . "</option>";
					}
				if (isset($_SESSION["hrAmt"]) and isset($_SESSION["minAmt"])) {
					echo "<td><input type=\"text\" name=\"hrAmt\" value=" .htmlspecialchars($_SESSION["hrAmt"]) . " ></td>";
					echo "<td><input type=\"text\" name=\"minAmt\" value=".htmlspecialchars($_SESSION["minAmt"]) . "></td>";
					unset($_SESSION["hrAmt"]);
					unset($_SESSION["minAmt"]);
				  } else {?>
					<td><input type="text" placeholder="Hours" name="hrAmt"></td>
					<td><input type="text" placeholder="Minutes" name="minAmt"></td>
				<?php } ?>
				<td><input type="submit" name="save" value="Save"> </td></tr>
			</form>
		</table>
			<?php if(isset($_SESSION["flash"])) { 
					echo "<div id=\"flash\">" . $_SESSION["flash"] . "</div>";
					unset($_SESSION["flash"]); 
			} ?>
		</div>

	</div>


<?php include 'footer.php'; ?>
