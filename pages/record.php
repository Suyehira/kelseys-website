<?php
	require_once "../dynamic/SessionHelper.php";
	ensure_logged_in();
	$user = unserialize($_SESSION["user"]);
	$thisPage = "record";
	include 'header.php';
?>
		
		<div class="content">

			<div class="top">
				Select a day to record your activities ...
			</div>
			
			<div id="datepicker-container">
				<div id="datepicker-center">
					<div id="calendar" name="calendar"></div>
				</div>
			</div>

		</div>


<?php include 'footer.php'; ?>

