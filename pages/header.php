


<html>

	<head>
		<link rel="stylesheet" type="text/css" href="../style.css" />
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /> 
		<script src="../jq/jquery-1.10.2.min.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="../jq/jCal.js"></script>
		<script src="../jq/cal.js"></script>
		<script src="../jq/date.js"></script>
		<script src="../jq/raphael/raphael-min.js"></script>
		<script src="../jq/raphael/g.raphael-min.js"></script>
		<script src="../jq/raphael/g.bar-min.js"></script>
		<script src="../jq/raphael/g.pie-min.js"></script>
		<script src="../jq/graphPie.js"></script>
		<script src="../jq/graphBar.js"></script>
	</head>

	<body>
		<div class="title">
			<a href="profile.php">Self Data</a>
		</div>
		<div id="nav-bar">
			<div class="logo">
    				<a href="profile.php"><img src="" /></a>
  			</div>
			<ul>
				<li <?php if ($thisPage=="profile") echo " id=\"currentPage\""; ?>>
					<a href="profile.php">Home</a>
				</li>
				<li<?php if ($thisPage=="record") echo " id=\"currentPage\""; ?>>
					<a href="record.php">Record</a>
				</li>
				<li class= 'last'<?php if ($thisPage=="display") echo " id=\"currentPage\""; ?>>
					<a href="displayPie.php">Display</a>
				</li>
			</ul>
		</div>

