<?php
	require_once "dynamic/SessionHelper.php";
?>	

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css" />
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500' rel='stylesheet' type='text/css'>
	</head>

	<body>
		<div class="title">
			<a href="index.php">Self Data</a>
		</div>
		<div id="nav-bar">
			<div class="logo">
    				<img src="" />
  			</div>
			<ul>
				<li class = "last">
				</br>
				</li>
			</ul>
		</div>

		<div class="content">
			<?php
			if(!isset($_SESSION)){
				session_start();
			}
			if(isset($_SESSION["flash"])) { 
				echo "<div id=\"flash\">" . $_SESSION["flash"] . "</div>";
				unset($_SESSION["flash"]); 
			} ?>

		<div class="signIn">
				<form action="dynamic/LogInHandler.php" method="POST">
					</br>Sign In
					<?php if (isset($_SESSION["SIemail"])){
						echo "</br><input type=\"text\" name=\"email\" value =" . htmlspecialchars($_SESSION["SIemail"]) . ">";
						unset($_SESSION["SIemail"]);
					} else { ?>
						 </br><input type="text" name="email" placeholder="email">
					<?php } ?>
					</br><input type="password" placeholder="Password" name="password">
					</br><input type="submit" name="signInB" value="Sign In">
				</form>
			</div>
			<div class="signUp">
				<form action="dynamic/SignUpHandler.php" method="POST">
					</br>Create a new account
					<?php if (isset($_SESSION["SUemail"])){
						echo "</br><input type=\"text\" name=\"email\" value =" . htmlspecialchars($_SESSION["SUemail"]) . ">";
						unset($_SESSION["SUemail"]);
					} else { ?>
						</br><input type="text" placeholder="Email" name="email">
					<?php } ?>
					</br><input type="password" placeholder="Password" name="password">
					</br><input type="submit" name="signUpB" value="Sign Up">
				</form>
			</div>
		</div>


		<div id="footer">
			&copy;2013 Kelsey Suyehira
		</div>

	</body>
</html>
