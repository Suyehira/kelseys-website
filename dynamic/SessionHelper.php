<?php

	date_default_timezone_set("America/Boise");
	if(!isset($_SESSION)) {session_start();}

	function ensure_logged_in(){
		if(!isset($_SESSION["user"])) {
			redirect("../index.php","Please log in.");
		}
	}


	function redirect($url,$flash_message){
		if ($flash_message){
			$_SESSION["flash"] = $flash_message;
		}
		header("Location:$url");
		die;
	}

?>
