
<?php

require_once "../classes/User.php";
require_once "../classes/Task.php";
require_once "../classes/Schedule.php";

class Dao {

	/*Need to hard code these variables to access the database*/
	private $host = "";
	private $db = "";
	private $user = "";
	private $pass = "";


	public function getConnection () {
		return new PDO("mysql:host={$this->host};dbname={$this->db}", $this->user,$this->pass);
	}


	public function getPassword($email) {
    		$conn = $this->getConnection();
    		$getQuery = "SELECT password FROM user WHERE email = :email;";
    		$q = $conn->prepare($getQuery);
    		$q->bindParam(":email", $email);
    		$q->execute();
    		return reset($q->fetchAll());	
	}


	public function password_correct($email,$password){
		$checkPass = $this->getPassword($email);
		if ($checkPass["password"] === $password){
			return true;
		} else {
			return false;
		}
	}


	public function getUserId($email) {
    		$conn = $this->getConnection();
    		$getQuery = "SELECT id FROM user WHERE email = :email;";
    		$q = $conn->prepare($getQuery);
    		$q->bindParam(":email", $email);
    		$q->execute();
    		return reset($q->fetchAll());	
	}

	public function isNewEmail($email){
		$conn = $this->getConnection();
    		$getQuery = "SELECT id FROM user WHERE email = :email;";
    		$q = $conn->prepare($getQuery);
    		$q->bindParam(":email", $email);
    		$q->execute();
		$boolArray = reset($q->fetchAll());
		if (isset($boolArray["id"])) {
			return false;
		} else {
			return true;
		}
	}

	public function addNewUser($user){
		$email = $user->getEmail();
		$password = $user->getPassword();
		$conn = $this->getConnection();
    		$saveQuery = "INSERT INTO user (email, password) VALUES (:email, :password);";
    		$q = $conn->prepare($saveQuery);
    		$q->bindParam(":email", $email);
		$q->bindParam(":password",$password);
    		$q->execute();
    		return reset($q->fetchAll());
	}
	
	public function getTasks($user_id) {
    		$conn = $this->getConnection();
    		$getQuery = "SELECT * FROM user_task JOIN task ON user_task.task_id = task.id WHERE user_id = :user_id;";
    		$q = $conn->prepare($getQuery);
    		$q->bindParam(":user_id", $user_id);
    		$q->execute();

    		$tasks = $q->fetchAll();
		$taskList = array();
		foreach ($tasks as $task) {
			$newTask = new Task($task["id"],$task["name"],$task["category"]);
			array_push($taskList,$newTask);
		}
		return $taskList;	
	}

	
	public function addTask($user, $name, $category){
		if ($this->isNewTask($name,$category)) {
			$conn = $this->getConnection();
			$saveQuery = "INSERT INTO task (name,category) VALUES (:name, :category);";
			$q = $conn->prepare($saveQuery);
			$q->bindParam(":name", $name);
			$q->bindParam(":category", $category);
			$q->execute();
		
			$task_id = $this->getTaskId($name,$category);
			$this->addUserTask($user->getId(),$task_id);

			$task = new Task($task_id, $name, $category);
			$user->addTask($task);

		} elseif ($this->isNewUserTask($user,$name,$category)) {
			$task_id = $this->getTaskId($name,$category);
			$this->addUserTask($user->getId(),$task_id);

			$task = new Task($task_id, $name, $category);
			$user->addTask($task);
		}
	}

	public function isNewUserTask($user,$name,$category) {
		$userId = $user->getId();
		$taskId = $this->getTaskId($name, $category);
		$temp = $this->getUserTaskInfo($userId,$taskId);
		if (isset($temp[0]["task_id"])) {
			return false;
		} else {
			return true;
		}	
	}

	public function getUserTaskInfo($userId, $taskId) {
		$conn = $this->getConnection();
		$getQuery = "select * from user_task join task on user_task.task_id = task.id where user_id = :userId and task_id = :taskId;";
		$q = $conn->prepare($getQuery);
		$q->bindParam(":userId", $userId);
		$q->bindParam(":taskId", $taskId);
		$q->execute();
		return $q->fetchAll();
	}


	public function getTaskInfo($name,$category) {
		$conn = $this->getConnection();
		$getQuery = "SELECT * FROM task WHERE name = :name AND category = :category;";
		$q = $conn->prepare($getQuery);
		$q->bindParam(":name", $name);
		$q->bindParam(":category",$category);
		$q->execute();
		return $q->fetchAll();
	}

	
	public function isNewTask($name,$category) {
		$task = $this->getTaskInfo($name,$category);
		if (isset($task[0]["id"])) {
			return false;
		} else {
			return true;
		}	
	}


	public function getTaskId($name,$category) {
		$task = $this->getTaskInfo($name,$category);
		return $task[0]["id"];
	}


	public function addUserTask($user_id,$task_id) {
		$conn = $this->getConnection();
		$saveQuery = "INSERT INTO user_task (user_id,task_id) VALUES (:user_id, :task_id);";
		$q = $conn->prepare($saveQuery);
		$q->bindParam(":user_id", $user_id);
		$q->bindParam(":task_id", $task_id);
		$q->execute();
	}

	public function getSched($user_id) {
   		$conn = $this->getConnection();
    		$getQuery = "SELECT * FROM schedule JOIN task ON schedule.task_id = task.id WHERE user_id = :user_id;";
    		$q = $conn->prepare($getQuery);
    		$q->bindParam(":user_id", $user_id);
    		$q->execute();
    		$scheds = $q->fetchAll();
		$schedList = array();
		foreach ($scheds as $sched) {
			$newSched = new Schedule($sched["day"],$sched["name"],$sched["task_id"],$sched["amount"]);
			array_push($schedList,$newSched);
		}
		return $schedList;	
	}

	
	public function addSched($user, $sched){
		$userId = $user->getId();
		if ($this->isNewSched($userId, $sched)) {
			$conn = $this->getConnection();
			$saveQuery = "INSERT INTO schedule (day, user_id, task_id, amount) VALUES (:day, :user_id, :task_id, :amount);";
			$q = $conn->prepare($saveQuery);
			$q->bindParam(":day", $sched->getDay());
			$q->bindParam(":user_id", $userId);
			$q->bindParam(":task_id", $sched->getTaskId());
			$q->bindParam(":amount", $sched->getAmount());
			$q->execute();
			$user->addSched($sched);
		}
	}


	public function getSchedInfo($userId,$sched) {
		$conn = $this->getConnection();
		$getQuery = "SELECT * FROM schedule WHERE day = :day AND user_id = :user_id AND task_id = :task_id;";
		$q = $conn->prepare($getQuery);
		$q->bindParam(":day", $sched->getDay());
		$q->bindParam(":user_id", $userId);
		$q->bindParam(":task_id", $sched->getTaskId());
		$q->execute();
		return $q->fetchAll();
	}

	
	public function isNewSched($userId, $sched) {
		$temp = $this->getSchedInfo($userId,$sched);
		if (isset($temp[0]["day"])) {
			return false;
		} else {
			return true;
		}	
	}

	public function deleteSched($user, $sched) {
		$conn = $this->getConnection();
		$delQuery = "DELETE FROM schedule WHERE day = :day AND user_id = :user_id AND task_id = :task_id;";
		$q = $conn->prepare($delQuery);
		$q->bindParam(":day", $sched->getDay());
		$q->bindParam(":user_id", $user->getId());
		$q->bindParam(":task_id", $sched->getTaskId());
		$q->execute();
		$user->deleteSched($sched);
	}

}

?>
