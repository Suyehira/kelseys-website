
<?php

require_once 'dao.php';
require_once "SessionHelper.php";
ensure_logged_in();
$user = unserialize($_SESSION["user"]);

	if(isset($_POST["save"])) {
		if (!empty($_POST["name"]) & !empty($_POST["category"])) {
			$dao = new Dao();
			$newName = $_POST["name"];
			$newCat = $_POST["category"];
			$dao->addTask($user, $newName, $newCat);
			$_SESSION["user"] = serialize($user);
		}
	}

	redirect("../pages/profile.php", "");

?>
