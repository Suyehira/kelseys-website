

<?php
session_start();

	if (isset($_POST["logOutB"])) {
		session_unset();
		session_destroy();
		session_regenerate_id(TRUE);
		session_start();
		header("Location:../index.php");
	} else {
		header("Location:../pages/profile.php");
	}

?>
