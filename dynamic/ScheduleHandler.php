<?php

require_once 'dao.php';
require_once "SessionHelper.php";
//require_once "../classes/User.php";
//require_once "../classes/Task.php";
require_once "../classes/Schedule.php";
ensure_logged_in();
$user = unserialize($_SESSION["user"]);
$day = $_SESSION["currDay"];
$url = "../pages/day.php?day=" . $day;
	
	if(isset($_POST["save"])) {
		if ( ($_POST["hrAmt"] ==="0" or $_POST["minAmt"] === "0") or 
				(!empty($_POST["hrAmt"]) & !empty($_POST["minAmt"]) ) ) {
			$dao = new Dao();
			$idLabel = explode('|', ($_POST["task"]));
			$taskId = $idLabel[0];
			$label = $idLabel[1];
			$hrAmt = $_POST["hrAmt"];
			$minAmt = $_POST["minAmt"];
			//use regEx to validate amounts
			preg_match("/(2[0-3])|([01]?[0-9])/",$hrAmt, $pregHr);
			preg_match("/(60)|([0-5]?\d)/", $minAmt, $pregMin);
			if(($pregHr[0] == $hrAmt) and ($pregMin[0] == $minAmt)) {
				$amount = $hrAmt*60 + $minAmt;
				$sched = new Schedule($day, $label, $taskId, $amount);
				$dao->addSched($user, $sched);
				$_SESSION["user"] = serialize($user);
			} else {
				$_SESSION["hrAmt"] = $hrAmt;
				$_SESSION["minAmt"] = $minAmt;
				redirect($url, "Invalid hour or minute amount");
			}
		}
	}

	if(isset($_POST["delete"])) {
		$taskId = $_POST["taskId"];
		$label = $_POST[$taskId];
		$dao = new Dao();
		$sched = new Schedule($day, $label, $taskId);
		$dao->deleteSched($user, $sched);
		$_SESSION["user"] = serialize($user);
	}
	
	redirect($url, "");

?>
